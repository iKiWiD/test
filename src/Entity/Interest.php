<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Interest
 *
 * @ORM\Table(name="interests")
 * @ORM\Entity(repositoryClass="App\Repository\InterestRepository")
 */
class Interest
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"show_interest", "list_interest"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="interests")
     * @ORM\JoinColumn(name="user_id", nullable=false)
     * @Groups({"show_interest", "list_interest"})
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="interests")
     * @ORM\JoinColumn(name="project_id", nullable=false)
     * @Groups({"show_interest", "list_interest"})
     */
    private $project;

    /**
     * @Assert\Positive
     * @ORM\Column(type="integer")
     * @Groups({"show_interest", "list_interest"})
     */
    private $amount;

    /**
     * Interest constructor.
     *
     * @param User $user
     * @param Project $project
     * @param int $amount
     */
    public function __construct(User $user, Project $project, int $amount)
    {
        $this->user = $user;
        $this->project = $project;
        $this->amount = $amount;

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}
