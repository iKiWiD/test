<?php

namespace App\Controller;

use App\Entity\Interest;
use App\Entity\Project;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProjectController extends AbstractController
{
    /**
     * @var EntityManagerInterface $_em
     */
    private $_em;

    /**
     * ProjectController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->_em = $em;
    }

    /**
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function index(SerializerInterface $serializer): JsonResponse
    {
        $projects = $this->getDoctrine()->getRepository(Project::class)->findAll();

        return (new JsonResponse())->setContent($serializer->serialize($projects, 'json'));
    }

    /**
     * @param Project $project
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     *
     * @return JsonResponse
     */
    public function addInterest(Project $project,
                                Request $request,
                                ValidatorInterface $validator,
                                SerializerInterface $serializer): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $interest = new Interest($user, $project, $request->request->get('amount', 0));

        $errors = $validator->validate($interest);
        if (count($errors) > 0) {
            $errorString = (string) $errors;
            throw new BadRequestHttpException($errorString);
        }

        $this->_em->persist($interest);
        $this->_em->flush();

        return (new JsonResponse([], Response::HTTP_CREATED))
            ->setContent($serializer->serialize($project, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]));
    }
}
