<?php

namespace App\Controller;

use App\Entity\Interest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\SerializerInterface;

class InterestController extends AbstractController
{
    /**
     * @var EntityManagerInterface $_em
     */
    public $_em;

    /**
     * InterestController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->_em = $em;
    }

    /**
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function index(SerializerInterface $serializer): JsonResponse
    {
        $interests = $this->getDoctrine()
            ->getRepository(Interest::class)
            ->findBy(['user' => $this->getUser()->getId()]);

        return (new JsonResponse())->setContent($serializer->serialize($interests, 'json', [
            'groups' => ['show_interest', 'list_project', 'list_user']
        ]));
    }

    /**
     * @param Interest $interest
     * @return Response
     */
    public function delete(Interest $interest): Response
    {
        $user = $this->getUser();
        if ($user !== $interest->getUser()) {
            throw new BadRequestHttpException();
        }
        $interest->getProject()->removeInterest($interest);
        $this->_em->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
