# Outils
* [Symfony5](https://symfony.com/5)

# Pour commencer

Pour lancer le projet vous aurez besoin de 
* [MAMP](https://documentation.mamp.info/en/MAMP-Mac/Installation/)
    - APACHE 2
    - MySQL 5.7
    - PHP 7.4
  
# Lancement
* Exectuer la commande "composer install"
* Exectuer les commandes suivantes :
  - php bin/console doctrine:database:create
  - php bin/console doctrine:schema:update --force
  - php bin/console doctrine:fixtures:load
  
* Le project est disponible à l'url :
  - http://localhost:8888/
  
# API (Postman)
- Vous pouvez importer la collection postman, disponible à la racine du projet